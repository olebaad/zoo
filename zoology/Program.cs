﻿using System;
using System.Collections.Generic;

namespace zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animalList = new List<Animal>();

            animalList.Add(new Animal());
            animalList.Add(new Animal("Blackie"));
            animalList.Add(new Animal("Sunshine", false, "Quokka"));

            int count = 1;
            foreach(Animal animal in animalList)
            {
                Console.WriteLine($"Animal no. {count}'s name is {animal.Name}.");
                Console.WriteLine($"The species of the animal is {animal.Species}");
                animal.WillItAttackOrNot();
                count++;
            }

            Whale whale = new Whale("Moby Dick", true, "Sperm Whale");
            Bird bird = new Bird("Pure Note", false, "Blue Tit");

            Console.WriteLine($"Animal no. {count}'s name is {whale.Name}.");
            Console.WriteLine($"The species of the animal is {whale.Species}");
            whale.HowDoesItGetAround();
            whale.WillItAttackOrNot();

            count++;
            Console.WriteLine($"Animal no. {count}'s name is {bird.Name}.");
            Console.WriteLine($"The species of the animal is {bird.Species}");
            bird.HowDoesItGetAround();
            bird.WillItAttackOrNot();


        }
    }
}
