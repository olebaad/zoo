﻿using System;

namespace zoology
{
	public class Animal
	{
		public string Name { get; set; }
		public string FeedType { get; set; }
		public string Species { get; set; }

		public Animal()
		{
			Species = "unknown";
			Name = "unknown";
			FeedType = "Omnivore";
		}


		public Animal(string name)
		{
			Species = "unknown";
			Name = name;
			FeedType = "Omnivore";
		}

		public Animal(bool eatsOnlyMeat, string species)
		{
			Species = species;
			Name = "unknown";
			if (eatsOnlyMeat)
			{
				FeedType = "Carnivore";
			}
			else
			{
				FeedType = "Herbivore";
			}
		}

		public Animal(string name, bool eatsOnlyMeat, string species)
		{
			Species = species;
			Name = name;
			if (eatsOnlyMeat)
			{
				FeedType = "Carnivore";
			}
			else
			{
				FeedType = "Herbivore";
			}
		}

		public virtual void WillItAttackOrNot()
		{
			Console.WriteLine($"Is the animal dangerous? \nIt is a {FeedType}:");
						
			if (FeedType == "Omnivore")
			{
				Console.WriteLine("Better safe than sorry: Back off slowly.\n");
			}
			else if (FeedType == "Carnivore")
			{
				Console.WriteLine("RUN!!!!!!!!!!!!\n");
			}
			else if (FeedType == "Herbivore")
			{
				Console.WriteLine("Chill out brah. Take a photo -^\n");
			}
		}
	}

	public abstract class Mammal : Animal
    {
		public string Reproduction { get; set; }

		public Mammal(bool eatsOnlyMeat, string species) : base(eatsOnlyMeat, species)
		{
			Reproduction = "give birth to live babies";
		}
		public Mammal(string name, bool eatsOnlyMeat, string species) : base(name, eatsOnlyMeat, species)
		{
			Reproduction = "give birth to live babies";
		}
	}

	public abstract class Oviparous : Animal
	{
		public string Reproduction { get; set; }

		public Oviparous(bool eatsOnlyMeat, string species) : base(eatsOnlyMeat, species)
		{
			Reproduction = "lay eggs";
		}
		public Oviparous(string name, bool eatsOnlyMeat, string species) : base(name, eatsOnlyMeat, species)
		{
			Reproduction = "lay eggs";
		}
	}

	public class Bird : Oviparous, IMovement
    {
		public Bird(string name, bool eatsOnlyMeat, string species) : base(name, eatsOnlyMeat, species) { }
		public Bird(bool eatsOnlyMeat, string species) : base(eatsOnlyMeat, species) { }

		public void HowDoesItGetAround()
        {
            if (Name == "unknown") {
				Console.WriteLine("The animal is a bird, so it gets around by flying.");
			}
			else {
				Console.WriteLine($"{Name} is a bird, so he/she gets around by flying.");
			}
		}

		public override void WillItAttackOrNot()
		{
			Console.WriteLine($"Is the animal dangerous? \nIt is a {FeedType}:");
			Console.WriteLine("Chill out brah, it's just a bird!\n");
		}
	}

	public class Whale : Mammal, IMovement
    {
		public Whale (string name, bool eatsOnlyMeat, string species) : base(name, eatsOnlyMeat, species) { }
		public Whale (bool eatsOnlyMeat, string species) : base(eatsOnlyMeat, species) { }
		public void HowDoesItGetAround()
		{
			if (Name == "unknown")
			{
				Console.WriteLine("The animal is a whale, so it gets around by swimming.");
			}
			else {
				Console.WriteLine($"{Name} is a whale, so he/she gets around by swimming.");
			}
		}

		public override void WillItAttackOrNot()
		{
			Console.WriteLine($"Is the animal dangerous? \nIt is a {FeedType}:");

			if (FeedType == "Omnivore")
			{
				Console.WriteLine("Better safe than sorry: Get back into the boat.\n");
			}
			else if (FeedType == "Carnivore")
			{
				Console.WriteLine("GET OUT OF THE WATER!!!!!!!!!!!\n");
			}
			else if (FeedType == "Herbivore")
			{
				Console.WriteLine("Chill out brah. Just avoid the tail -^\n");
			}
		}
	}

	public interface IMovement
    {
		public void HowDoesItGetAround();
    }
}
